/*
 * Copyright (C) 2018 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

#include <string.h>

#include "board.h"

#include "net/loramac.h"
#include "semtech_loramac.h"

/* Declare globally Semtech SX1276 radio device descriptor */

/* Device and application informations required for OTAA activation */


int main(void)
{
    /* setup loramac interface */

    /* start the OTAA join procedure */

    /* change datarate to DR5 (SF7/BW125kHz) */

    while (1) {
        /* char *message = "This is RIOT!"; */

        /* send the message every 20 seconds */

        /* wait 20 seconds between each message */
        xtimer_sleep(20);
    }

    return 0; /* should never be reached */
}
