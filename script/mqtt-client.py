"""MQTT client for TTN"""

# Copyright (c) 2018 Inria

import os.path
import json
import base64
import paho.mqtt.client as mqtt

USERNAME = '<your TTN application name>'
PASSWORD = '<your TTN application access key>'
DEVICE_NAME = '<your device name>'
CA_FILE_PATH = os.path.expanduser('~/mqtt-ca.pem')

TTN_BROKER_ADDR = 'eu.thethings.network'
TTN_BROKER_PORT = 8883

SUBSCRIBE_TOPIC = '{}/devices/{}/up'.format(USERNAME, DEVICE_NAME)
PUBLISH_TOPIC = '{}/devices/{}/down'.format(USERNAME, DEVICE_NAME)


def on_message(client, userdata, msg):
    """Callback triggered for each message received from the server."""
    print("Message received on topic: {}".format(msg.topic))
    print(msg.payload)


def on_connect(client, userdata, flags, rc):
    """Callback triggered after the connection to the broker."""
    print('Connected to TTN broker, waiting for incoming messages')

    # Now that we are connected, we can subscribe to the device uplink topic.
    client.subscribe(SUBSCRIBE_TOPIC)


def on_disconnect(client, userdata, rc):
    """Callback triggered the client is disconnected from the broker."""
    print('Disconnected from TTN broker')


def start():
    """Create the client and connect it to the broker."""
    client = mqtt.Client()
    client.tls_set(CA_FILE_PATH)
    client.username_pw_set(USERNAME, password=PASSWORD)
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message
    client.connect(TTN_BROKER_ADDR, port=TTN_BROKER_PORT, keepalive=60)

    return client


if __name__ == '__main__':
    try:
        client = start()
        client.loop_forever()
    except KeyboardInterrupt:
        # Smoothly close the connection to the broker
        client.disconnect()
